import mongoose from "mongoose"
import crypto from "crypto"
import jwt from "jsonwebtoken"
import db from "../db"
import Project from "./Project"

const UserSchema = new mongoose.Schema({
    email: String,
    hash: String,
    salt: String,
    settings: {
        alias: String,
        locale: {
            type: String,
            default: "pl-PL"
        },
        theme: {
            type: String,
            default: "light"
        },
    },
    // wakeUpTaskIds: {
    //     type: [ mongoose.Schema.ObjectId ],
    //     default: []
    // },
    // projectOrderIds: {
    //     type: [ mongoose.Schema.ObjectId ],
    //     default: []
    // },
    projects: {
        type: [ Project.Schema ],
        default: []
    }
}, { 
    collection: "users"
})

UserSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
}

UserSchema.methods.validatePassword = function(password) { 
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
}

UserSchema.methods.generateJWT = function() {
    const today = new Date()
    const expirationDate = new Date(today)
    expirationDate.setDate(today.getDate() + 60)

    return jwt.sign({
        email: this.email,
        id: this._id,
        exp: parseInt(expirationDate.getTime() / 1000, 10)
    }, "secret")
}

UserSchema.methods.toAuthJSON = function() {
    return {
        _id: this._id,
        email: this.email,
        token: this.generateJWT()
    }
}

export default {
    Model: db.model("User", UserSchema),
    Schema: UserSchema
}