import mongoose from "mongoose"
import db from "../db"
import Task from "./Task"

const ProjectSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        max: 150
    },
    tasks: {
        type: [ Task.Schema ],
        default: []
    },
    // taskOrderIds: {
    //     type: [ mongoose.Schema.ObjectId ],
    //     default: []
    // }
})

export default {
    Schema: ProjectSchema,
    Model: db.model("Project", ProjectSchema)
}