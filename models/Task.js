import mongoose from "mongoose"
import db from "../db"

const TaskSchema = new mongoose.Schema({
    text: {
        type: String,
        required: true,
        max: 500
    },
    isDone: {
        type: Boolean,
        default: false
    },
    due: Date
})

export default {
    Model: db.model("Task", TaskSchema),
    Schema: TaskSchema
}