import jwt from "express-jwt"
import db from "./db"

const getTokenFromHeaders = req => {
    const { headers: { authorization } } = req
    // ADD userID to token
    return authorization ? authorization : null
}

const auth = {
    required: jwt({
        secret: "secret",
        userProperty: "payload",
        getToken: getTokenFromHeaders
    }),
    optional: jwt({
        secret: "secret",
        userProperty: "payload",
        getToken: getTokenFromHeaders,
        credentialsRequired: false
    })
}

export default auth