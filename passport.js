import mongoose from "mongoose"
import passport from "passport"
import LocalStrategy from "passport-local"

import { UserModel } from "./models"

const handleAuth = passport.use(new LocalStrategy({
    usernameField: "user[email]",
    passwordField: "user[password]"
}, (email, password, done) => {
    UserModel.findOne({ email }).then(user => {
        if (!user || !user.validatePassword(password)) {
            return done(null, false, { error: "Password is invalid or there is no such account." })
        }

        return done(null, user)
    })
}))

export default handleAuth