import express from "express"
import path from "path"
import cookieParser from "cookie-parser"
import logger from "morgan"
import cors from "cors"
import db from "./db"
import session from "express-session"
import passport from "passport"
import { setResponseHeaders, acceptProperHeaders } from "./middlewares"
import { ProjectRouter, TaskRouter, UserRouter } from "./routes"

const app = express()

app.use(cors())
app.use(logger("dev"))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
// app.use([acceptProperHeaders, setResponseHeaders])
app.use(session({ 
    secret: "secret", 
    cookie: { maxAge: 60000, secure: false }, 
    resave: true, 
    saveUninitialized: false 
}))

app.use(passport.initialize())
app.use(passport.session())

app.use(cookieParser())

app.use(express.static(path.join(__dirname, "public")))


app.use("/", UserRouter)
app.use("/projects", ProjectRouter)
app.use("/tasks", TaskRouter)

module.exports = app

// TODO: Update position of project, handle "due" key in task.
// TODO: End projects controllers and middlewares, fix query problems with search tasks.