import express from "express"
import { TaskController } from "../controllers"
import auth from "../auth"
import { User } from "../models"

const router = express.Router()

const authCurrentUser = (req, res, next) => User.Model.findById(req.payload.id).then(user => {
	if (!user) {
		return res.redirect("/login")
	}

	return next()
})

router.use("/", auth.required, authCurrentUser, TaskController.checkProjectId)

router.get("/", TaskController.prepareSearchParams, TaskController.getAll)
router.post("/", TaskController.create)
router.delete("/", TaskController.remove)
router.patch("/", TaskController.update)

export default router
