import express from "express"
import { UserController } from "../controllers"
import auth from "../auth"
const router = express.Router()

router.post("/create", auth.optional, UserController.create)
router.post("/login", auth.optional, UserController.login)
router.post("/", auth.required, UserController.authCurrentUser)

export default router
