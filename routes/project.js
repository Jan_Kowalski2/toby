import express from "express"
import { ProjectController } from "../controllers"
import auth from "../auth"
import { Project, User } from "../models"

const router = express.Router()

const handleProjectId = (req, res, next) => {
    const { projectId } = req.body

    if (!req.body.projectId) {
        return res.status(404).json({ error: "No project found." })
    }

    return Project.Model.findById({}, (error, projects) => {
		if (error) {
			return res
				.status(500)
				.json({ error: "Couldn't return project from database." })
		}
		return res.status(200).json(projects)
	})

    return Project.Model.findById(projectId)
}

const authCurrentUser = (req, res, next) => User.Model.findById(req.payload.id).then(user => {
	if (!user) {
		return res.redirect("/login")
	}

	return next()
})

router.use("/", auth.required, authCurrentUser)

router.get("/", auth.required, ProjectController.getAll)
router.post("/", auth.required, ProjectController.create)
router.delete("/", auth.required, ProjectController.remove)
router.patch("/", auth.required, ProjectController.update)

export default router
