import mongoose from "mongoose";
import { DB } from "./constants"

const uri = `mongodb+srv://${DB.USER}:${DB.PASS}@${DB.ADDR}`;
mongoose.connect(uri, { useNewUrlParser: true })

const connection = mongoose.connection

connection.createCollection("users")
connection.createCollection("projects")

mongoose.promise = global.Promise

export default connection 
