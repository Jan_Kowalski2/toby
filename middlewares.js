import { HEADERS } from "./constants"

export const setResponseHeaders = (req, res, next) => {
	res.setHeader("charset", "utf-8")
	res.setHeader("Content-Type", HEADERS["Content-Type"])
	return next()
}

export const acceptProperHeaders = (req, res, next) => {
	if (req.header("Content-Type") !== HEADERS["Content-Type"]) {
		return res
			.status(406)
			.json({ error: `Data should be sent via ${HEADERS["Content-Type"]}.` })
	}

	return next()
}