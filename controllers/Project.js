import { User, Project } from "../models"

const getAll = (req, res) => {
	const { id: userId } = req.payload
	return User.Model.findById(userId, (error, user) => {
		if (error) {
			return res
				.status(500)
				.json({ error: "Couldn't return projects from database." })
		}
		return res.status(200).json(user.projects)
	})
}

const create = (req, res) => {
	const { title } = req.body
	const { id: userId } = req.payload

	if (!title || title.length === 0) {
		return res.status(422).json({ error: "Title is required." })
	}

	const nextProject = new Project.Model({ title })

	User.Model.findByIdAndUpdate(
		userId,
		{ $push: { projects: nextProject } }, 
		{ new: true, useFindAndModify: false }, 
		(error) => {
		if (error) {
			return res
				.status(500)
				.json({
					error: "Project couldn't be created because of database failure."
				})
		}
		return res.status(200).send(nextProject)
	})
}

const remove = (req, res) => {
	const { projectId } = req.body
	const { id: userId } = req.payload

	User.Model.findByIdAndUpdate(
		userId,
		{ $pull: { projects: { _id: projectId } } },
		{ new: true, useFindAndModify: false }, 
		error => {
		if (error) {
			return res.status(404).json({ error: "Project not found!" })
		}

		return res.status(204).send()
	})
}

const update = (req, res) => {
	const { title, projectId } = req.body
	const { id: userId } = req.payload

	User.Model.findOneAndUpdate(
		{ "_id": userId, "projects._id": projectId },
		{ $set: { "projects.$.title": title } },
		{ new: true, useFindAndModify: false }, 
		(error, user) => {
		if (error) {
			return res.status(404).json({ error: "Project not found!" })
		}

		const nextProject = user.projects.find(el => el._id.toString() === projectId)

		return res.status(200).json(nextProject)
	})
}

export default {
    getAll,
    remove,
    update,
    create
}