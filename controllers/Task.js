import { Task, User } from "../models"

const prepareSearchParams = (req, res, next) => {
	const { query = "", isDone } = req.query
	const isTaskDone = isDone === "true" ? true : isDone === "false" ? false : undefined
	const searchParams = {
		text: new RegExp(`.*${query}.*`),
		isDone: isTaskDone
	}

	if (query.length > 0 && query.length <= 3) {
		return res.status(422).json({ error: "Query should be longer than 3 characters."})
	}
	
	!isTaskDone && delete searchParams.isDone 
	query.length < 3 && delete searchParams.text 

	req.body.searchParams = searchParams
	next()
}

const checkProjectId = (req, res, next) => {
	const { projectId } = req.body

	if (!projectId) {
		return res.status(422).json({ error: "Project ID is missing."})
	}

	return next()
}

const getAll = (req, res) => {
	const { projectId } = req.body
	const { id: userId } = req.payload
		
	User.Model.findOneAndUpdate(
		{ "_id": userId, "projects._id": projectId },
		{ useFindAndModify: false }, 
		(error, user) => {
		if (error) {
			return res.status(404).json({ error: "Project not found!" })
		}

		const project = user.projects.find(el => el._id.toString() === projectId)
		
		return res.status(200).json(project.tasks)
	})
}

const create = (req, res) => {
	const { text, projectId } = req.body
	const { id: userId } = req.payload
	
	if (!text || text.length === 0) {
		return res.status(422).json({ error: "Text is missing." })
	}

	const nextTask = new Task.Model({ text })

	User.Model.findOneAndUpdate(
		{ "_id": userId, "projects._id": projectId },
		{ $push: { "projects.$.tasks": nextTask } }, 
		{ new: true, useFindAndModify: false }, 
		(error) => {
		if (error) {
			return res
				.status(500)
				.json({
					error: "Task couldn't be created because of database failure."
				})
		}
		return res.status(200).send(nextTask)
	})
	
}

const remove = (req, res) => {
	const { projectId, taskId } = req.body
	const { id: userId } = req.payload

	if (!taskId) {
		return res.status(422).json({ error: "Task ID is missing."})
	}

	User.Model.findOneAndUpdate(
		{ "_id": userId, "projects._id": projectId, "projects.tasks._id": taskId },
		{ $pull: { "projects.$.tasks": { _id: taskId } } },
		{ useFindAndModify: false }, 
		(error, user) => {

		if (error) {
			return res.status(404).json({ error: "Task not found!" })
		}

		return res.status(204).send()
	})
}

const update = (req, res) => {
	const { projectId, taskId, isDone, text } = req.body
	const { id: userId } = req.payload

	if (typeof isDone === "undefined") {
		return res.status(422).json({ error: "Task status is missing (isDone key)."})
	}

	if (typeof text === "undefined") {
		return res.status(422).json({ error: "Task's text is missing."})
	}

	if (text.length === 0) {
		return res.status(422).json({ error: "Task should contain text."})
	}

	if (!taskId) {
		return res.status(422).json({ error: "Task ID is missing."})
	}

	User.Model.findOneAndUpdate(
		{ "_id": userId, "projects._id": projectId, "projects.tasks._id": taskId },
		{ $set: { "projects.$.tasks": { isDone, text, _id: taskId } } },
		{ new: true, useFindAndModify: false }, 
		(error, user) => {

		if (error) {
			return res.status(404).json({ error: "Task not found!" })
		}

		const project = user.projects.find(el => el._id.toString() === projectId)
		const nextTask = project.tasks.find(el => el._id.toString() === taskId)
		
		return res.status(200).send(nextTask)
	})
}

export default {
	prepareSearchParams,
	checkProjectId,
    getAll,
    remove,
    update,
    create
}