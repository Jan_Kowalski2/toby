import passport from "passport"
import { User } from "../models"

const create = (req, res) => {
	const {
		body: { user }
	} = req

	if (!user.email) {
		return res.status(422).json({ error: "Email is required." })
	}

	if (!user.password) {
		return res.status(422).json({ error: "Password is required." })
	}

	const nextUser = new User.Model(user)
	nextUser.setPassword(user.password)

	return nextUser.save().then(() => res.json({ user: nextUser.toAuthJSON() }))
}

const login = (req, res, next) => {
	const {
		body: { user }
	} = req

	if (!user.email) {
		return res.status(422).json({ error: "Email is required." })
	}

	if (!user.password) {
		return res.status(422).json({ error: "Password is required." })
	}

	return passport.authenticate(
		"local",
		{ session: false },
		(error, passportUser, info) => {
			if (error) {
				return next(error)
			}

			if (passportUser) {
				const user = passportUser
				user.token = passportUser.generateJWT()

				return res.json({ user: user.toAuthJSON() })
			}

			return res.status(400).json({ error: info.error })
		}
	)(req, res, next)
}

const authCurrentUser = (req, res) => User.Model.findById(req.payload.id).then(user => {
	if (!user) {
		return res.redirect("/login")
	}

	return res.status(200).send(user)
})

export default {
	create,
	login,
	authCurrentUser
}
